# Base16 Themes

This folder contains the source files and scripts used to convert the Base16 Themes found in Highlight's "[`/themes/base16/`][HL base16]" folder.


-----

**Table of Contents**

<!-- MarkdownTOC autolink="true" bracket="round" autoanchor="false" lowercase="only_ascii" uri_encoding="true" levels="1,2,3" -->

- [Files and Folders](#files-and-folders)
- [Themes Status](#themes-status)
- [About Base16](#about-base16)
- [Building the Themes](#building-the-themes)
    - [Requirements](#requirements)
    - [Porting the Scripts & Using the Template](#porting-the-scripts-using-the-template)
- [External Links](#external-links)

<!-- /MarkdownTOC -->

-----

# Files and Folders


- [`/schemes/`][schemes] — input folder: Base16 YAML source schemes.
- [`/themes/`][themes] — output folder: converted Highlight Base16 themes.
- [`base16_highlight.mustache`][base16_highlight.mustache] — [mustache] theme template.
- [`CONVERT.bat`][CONVERT.bat] — script for batch coversion.
- [`DEPLOY.bat`][DEPLOY.bat] — script for copying converted themes to Highlight repo.


# Themes Status

This project contains all the 107 __base16__ schemes found in the [Scheme Repositories] listed in the [base16 project] as of commit [`dce99f6`][dce99f6] (2018/05/30).

All themes were commited to Highlight's "[`/themes/base16/`][HL base16]" folder via commit [`6ce45137`][6ce45137] (2018-06-20) and will be available in all Highlight `>=3.44` distributions.

[6ce45137]: https://gitlab.com/saalen/highlight/commit/6ce451372f1b76ec30fba3c8c98d190588de12a2 "View commit '6ce45137' to Highlight on GitLab (2018-06-20)"

# About Base16

- http://chriskempson.com/projects/base16/

From the [base16 homepage]:

> \[Base16 is\] an architecture for building themes based on carefully chosen syntax highlighting using a base of sixteen colours. Base16 provides a set of guidelines detailing how to style syntax and how to code a _builder_ for compiling base16 _schemes_ and _templates_.

# Building the Themes

To convert the Base16 YAML schemes, I've taken a custom approach instead of using one of on the official [base16 builders]. This approach has both pros and cons, so other might prefer to use the mustache template with a builder instead.

There various reasons why I didn't want to use an official base16 builder:

1. I dediced to add a extra `url` variable to the YAML schemes, pointing to the repository the scheme was downloaded from, so that it could be included in the final themes' comments.

2. My mustache template is designed for the original (old) base16 naming convention — which is the one still used in most color schemes found around the web — while most builders use the newer conventions (base16 `v0.8.0`–`v0.9.0`), which rely on a different naming convention for template variables.

3. The builders will download and use bezhermoso's __[base16-highlight]__ template, which is the officialy registered template for Highlight themes. The problem with his template is that [it contains variable naming problems], including a mispelled theme variable which compromises the converted themes; and the project seems stale. Also, I've taken a different approach from bezhermoso in assigning the 16 colors to theme elements, which I believe to be more in line with the [Base16 Styling Guidelines].

For the above reasons, a custom workflow seemed a better idea. The downside is that updating the base16 schemes has to be done manually, by checking from time to time if new schemes were added to the list.

In the future, I might try to find a better solution; but right now I'm quite satisfied that this approach allowed the inclusion of 107 new themes at once in the Highlight project.


## Requirements

Since I'm only interested in converting the YAML schemes to Highlight themes, all I really needed where two Node.js CLI tools:

- __[mustache.js]__ (by Jan Lehnardt) — JavaScript implementation of [mustache].
- __[yaml-utils]__ (by Christopher Brown) — to convert from YAML to JSON.

In order to build the themes via the scripts in this project, you'll need to install these two packages globally on your machine (obviously, you'll need [Node.js] too).

Of course, you can instead use any mustache tool of your choice to convert the themes, the [`base16_highlight.mustache`][base16_highlight.mustache] will work with any mustache implementation.

## Porting the Scripts & Using the Template

Currently, all scripts are Windows only. Since they are rather short scripts, converting them to Shell scripts should be quite simple and straight forward.

All the script does, it passes the YAML source scheme to `yaml2json`, pipes the JSON output to `mustache` (invoked with `base16_highlight.mustache` as template) and redirects the output to file:

```dos
yaml2json < %1 | mustache - base16_highlight.mustache  > "%OUT_DIR%\%~n1.theme"
```

... which is the equivalent of the single CMD command:

```dos
yaml2json < filename.yaml | mustache - base16_highlight.mustache  > filename.theme
```


# External Links

- [base16 homepage]
- [base16 repository] — at GitHub
- [mustache homepage][mustache]
- [mustache.js] — at NPM
- [yaml-utils] — at NPM

<!-----------------------------------------------------------------------------
                               REFERENCE LINKS
------------------------------------------------------------------------------>

<!--------------------------- Internal Repo Links ---------------------------->

[schemes]: ./schemes
[themes]: ./themes
[base16_highlight.mustache]: ./base16_highlight.mustache
[CONVERT.bat]: ./CONVERT.bat
[DEPLOY.bat]: ./DEPLOY.bat

<!----------------------------- Highlight Links ------------------------------>

[HL base16]: https://gitlab.com/saalen/highlight/tree/master/themes/base16 "See the '/themes/base16/' folder in Highlight’s source repository on GitLab"

<!--------------------------- External References ---------------------------->

[mustache]: https://mustache.github.io/ "Visit mustache website"

<!-- Base16 Official -->

[base16 homepage]: http://chriskempson.com/projects/base16/ "Visit base16 homepage"
[base16 repository]: https://github.com/chriskempson/base16 "Visit base16 project on GitHub"
[base16 project]: https://github.com/chriskempson/base16 "Visit base16 project on GitHub"
[base16 builders]: https://github.com/chriskempson/base16#builder-repositories
[Base16 Styling Guidelines]: http://chriskempson.com/projects/base16/#styling-guidelines "Read the Base16 Styling Guidelines"

[Scheme Repositories]: https://github.com/chriskempson/base16#scheme-repositories "See the list of Scheme Repositories mentioned in the base16 official project"
[dce99f6]: https://github.com/chriskempson/base16/blob/dce99f6/README.md#scheme-repositories

<!-- Bae16 Other -->

[base16-highlight]: https://github.com/bezhermoso/base16-highlight "Visit bezhermoso's 'base16-highlight' project on GitHub"
[it contains variable naming problems]: https://github.com/bezhermoso/base16-highlight/issues/1 "View the open issue on bezhermoso's 'base16-highlight' template errors"

<!-- Dependencies -->

[mustache.js]: https://www.npmjs.com/package/mustache "Visit 'mustache.js' page on NPM"
[yaml-utils]: https://www.npmjs.com/package/yaml-utils "Visit 'yaml-utils' page on NPM"
[Node.js]: https://nodejs.org/ "Visit Node.js website"

<!-- eof -->
