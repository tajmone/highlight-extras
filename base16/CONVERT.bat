@ECHO OFF
CLS
ECHO ==============================================================================
ECHO                   Batch Base16 to Highlight Theme Converter
ECHO ==============================================================================
ECHO by Tristano Ajmone ^| v1.0.3 (2018/06/19) ^| MIT License
:: -----------------------------------------------------------------------------
:: This script requires the following Node.js packages to be installed globally:
::
::   mustache.js (by Jan Lehnardt):
::   https://www.npmjs.com/package/mustache
::
::   yaml-utils (by Christopher Brown)
::   https://www.npmjs.com/package/yaml-utils
:: -----------------------------------------------------------------------------
SET SRC_DIR=schemes
SET OUT_DIR=themes
:: -----------------------------------------------------------------------------
:: Delete all pre-existing theme files in destination folder:
DEL /Q %OUT_DIR%\*.*
:: -----------------------------------------------------------------------------
FOR %%i IN (%SRC_DIR%/*.yaml) DO (
    CALL :Conv2Theme "%SRC_DIR%/%%i"
)
EXIT /B

:: =============================================================================
:: Function | Convert from Base16 YAML to Hihglight Theme
:: =============================================================================
:Conv2Theme
ECHO ------------------------------------------------------------------------------
ECHO PROCESSING: %~nx1
ECHO CONVERTING: %~n1.theme
yaml2json < %1 | mustache - base16_highlight.mustache  > "%OUT_DIR%\%~n1.theme"
EXIT /B

:: -----------------------------------------------------------------------------
:: The MIT License
::
:: Copyright (c) 2018 Tristano Ajmone: <tajmone@gmail.com>
::                                     https://github.com/tajmone
::                                     https://gitlab.com/tajmone
::
:: Permission is hereby granted, free of charge, to any person obtaining a copy
:: of this software and associated documentation files (the "Software"), to deal
:: in the Software without restriction, including without limitation the rights
:: to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
:: copies of the Software, and to permit persons to whom the Software is
:: furnished to do so, subject to the following conditions:
::
:: The above copyright notice and this permission notice shall be included in all
:: copies or substantial portions of the Software.
::
:: THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
:: IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
:: FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
:: AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
:: LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
:: OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
:: SOFTWARE.
:: -----------------------------------------------------------------------------
