:: "base16\DEPLOY.bat" v1.0.2 (2018/06/20) | by Tristano Ajmone
:: -----------------------------------------------------------------------------
:: Released into the public domain via the Unlicense terms:
::     http://unlicense.org/
:: -----------------------------------------------------------------------------
:: Copies all converted themes to Highlight's repo's "themes\base16\".
:: This script assumes that the "highlight-extras" repository is placed in the
:: same folder as the Highligt repository -- if not, you'll need to adjust the
:: path in %DST_DIR% accordingly.
:: -----------------------------------------------------------------------------
SET SRC_DIR=themes
SET DST_DIR=..\..\highlight\themes\base16

COPY "%SRC_DIR%\*.theme" "%DST_DIR%\"
