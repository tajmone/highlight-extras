# Tristano's Extra Resources for Highlight

- https://gitlab.com/tajmone/highlight-extras

This project contains the source files of my contributed resources to the [Highlight] project, plus some other useful resources that are either not included in Highlight distribution or that I'm still working on.

The various resources have different license terms, make sure to consult the README file of each folder and the comments in the resources' source files.

> __*WIP NOTICE*__ — This project is currently in an early work-in-progress stage; I'm still reorganizing my old resources in order to port them to the unified workflow of this repository. It might take same time before I adapt all the old scripts to the new system.

-----

**Table of Contents**

<!-- MarkdownTOC autolink="true" bracket="round" autoanchor="false" lowercase="only_ascii" uri_encoding="true" levels="1,2,3" -->

- [Project Structure](#project-structure)
- [About This Project](#about-this-project)
- [Other Resources](#other-resources)
    - [Learning Resources](#learning-resources)
    - [Syntax Definitions](#syntax-definitions)

<!-- /MarkdownTOC -->

-----


# Project Structure

- [`/_assets/`][assets] — assets commonly shared by the various resources.
- [`/base16/`][base16] — sources of my contributed contents to Highlight's "[`/base16/`][HL base16]" folder.
- [`/extras/`][extras] — sources of my contributed contents to Highlight's "[`/extras/`][HL extras]" folder.


# About This Project

During the course of time I've contributed various resource to the Highlight project. Some of these resources, like those found in Highlight's "[`/extras/`][HL extras]" folder, included some HTML documentation which I built locally from markdown files, as well as some other source files which were used to build the final contents.

As the number of resources started to grow in number, maintaining the various assets and scripts used for building these resources independently became more complicated, so I decided to gather them together under a unified workflow where they can share the same assets and scripts for converting their documentation files and other common aspects.

This not only simplifies maintainance of the various assets (pandoc templates, pp macros, scripts, etc.), it also enforces a consistent look and design to the resources themselves.

Hopefully, the various assets and scripts of this project might also turn out to be useful to others willing to contribute their own resources to Highlight — feel free to copy them and adapt them to your needs.

# Other Resources

I've also created some other Highlight resources, maintained separately:

## Learning Resources

I'm maintaining a Wiki for learning how to use Highlight and create custom syntax definitions and themes:

- [Tristano's Wiki for Highlight]

## Syntax Definitions

Syntax definitions which I've created for Highlight:

- [GDScript]
- [FASM]
- [PureBasic]

<!-----------------------------------------------------------------------------
                               REFERENCE LINKS
------------------------------------------------------------------------------>

<!--------------------------- Internal Repo Links ---------------------------->

[assets]: ./_assets
[base16]: ./base16
[extras]: ./extras/

<!----------------------------- Highlight Links ------------------------------>

[Highlight]: http://www.andre-simon.de/doku/highlight/en/highlight.php "Visit Highlight’s homepage at www.andre-simon.de"

[HL GitLab]: https://gitlab.com/saalen/highlight "Visit Highlight’s source repository on GitLab"

[HL extras]: https://gitlab.com/saalen/highlight/tree/master/extras "See the '/extras/' folder in Highlight’s source repository on GitLab"
[HL base16]: https://gitlab.com/saalen/highlight/tree/master/themes/base16 "See the '/themes/base16/' folder in Highlight’s source repository on GitLab"

<!-------------------------- My Other HL Resources --------------------------->

[Tristano's Wiki for Highlight]: https://github.com/tajmone/highlight/wiki "Visit Tristano's Highlight Wiki on GitHub"
[GDScript]: https://github.com/tajmone/gdscript-highlight "Go to the GDScript langDef repository on GitHub"
[FASM]: https://github.com/tajmone/fasm-highlighting "Go to the FASM langDef repository on GitHub"
[PureBasic]: https://github.com/tajmone/purebasic-archives/tree/master/syntax-highlighting/highlight "View the PureBasic langDef project at 'The PureBASIC Archives'"

<!-- eof -->
