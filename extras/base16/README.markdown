---
# !def(date_creation)(2018-07-12)
# !def(date_modified)(2018-07-13)
title:      Base16 to Highlight Themes Templates
toc-title:  CONTENTS
summary: |
    ```
    Last edited: !date_modified | Creation date: !date_creation
    ```

    Written by [Tristano Ajmone], Copyright © 2018, this document is
    [released under MIT License] (MIT).

    Latest mustache templates, __v1.0.0__ (2018/07/12):

    - [`base16_highlight.mustache`][b16HL]
    - [`base16_highlight_light.mustache`][b16HL light]


    The templates are [MIT License].

lang: en
pagetitle:   Base16 Highlight Themes
author-meta: Tristano Ajmone
date-meta:   !date_creation
css: swatches.css
...


!comment(ABOUT THIS DOCUMENT)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This is a PP -> Pandoc-Markdown document that requiresthe pp-macros
library from the "Pandoc Goodies" project:

https://github.com/tajmone/pandoc-goodies/tree/master/pp

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# Introduction

Base16 was invented by Chris Kempson.


## What Is Base16?

From the [Base16 website]:


> An architecture for building themes based on carefully chosen syntax highlighting using a base of sixteen colours. Base16 provides a set of guidelines detailing how to style syntax and how to code a builder for compiling base16 _schemes_ and _templates_.

In other words, Base16 is a standard way of coding color schemes with a palette of 16 colors into YAML files; but it's also a collection of [mustache] templates and various tools for programmatically converting color schemes into usable settings files for various tools.

While Chris Kempson designed the original Base16 color schemes, templates and scripts, Base16 soon gathered momentum and attracted many contributors to the project, shifting the focus from the original project files to its guidelines, making Base16 a common standard used for the constantly growing number of color schemes, templates and tools created and maintained by independent developers.

Today there are many tools (in various scripting languages) for managing and converting color schemes and templates, and the official [Base16 repository] acts as a central reference gathering links to available resources.

## What's in This Folder?

Here you'll find some [mustache] templates for building Highlight themes from Base16 color schemes (distributed as YAML files).

These templates were created by Tristano Ajmone and released under the [MIT License].


# Files Contents

Theme templates:

- [`base16_highlight.mustache`][b16HL] — base theme template
- [`base16_highlight_light.mustache`][b16HL light] — light theme template (for schemes that support a light colors version)

Example files:

- [`example.bat`][ex.bat] — script to build examples
- [`example.pb`][ex.pb] — sample source code to highlight
- [`example.yaml`][ex.yaml] — Base16 "Eighties" color scheme (by [Chris Kempson], MIT License)
- [`example.theme`][ex.theme] — Base16 "Eighties" converted theme (dark)
- [`example_light.theme`][ex-light.theme] — Base16 "Eighties" converted theme (light)
- [`example-dark.html`][ex-dark.html] — highlighting example (dark theme)
- [`example-light.html`][ex-light.html] — highlighting example (light theme)



# Using the Templates

Please, note that the current templates were created for the original (older) Base16 standard, because it's still the most used one and almost all color schemes you'll find on the internet are likely to be using this version. 

The new standard of Base16 (v0.9.0) uses different variable names in both the schemes and mustache templates, as well as adding a few new variables (which are not relevant for creating Highlight themes). Adapting the templates in this folder to the new standard only requires tweaking a few variables names.

## Dark vs Light Template

The base template is `base16_highlight.mustache`, this is the template you will use in most circumstances. 

Some color schemes are designed to work well by inverting the order of the colors from `base00` to `base07`; in such cases the base version is usually the _dark_ version, while the one with the inverted color range is the _light_ version. With such color schemes you can use also the `base16_highlight_light.mustache` to produce a theme with the inverted color range `base00` to `base07` from the same scheme.

Some color schemes already ship in a dark and light version (ie, as two YAML files, usually named `<schemename>_dark.yaml` and `<schemename>_light.yaml`); in this case you should use only the `base16_highlight.mustache` template with both. In all other cases, you can always try to use both conversion templates and see if the light theme version looks good or not.


## Mustache

To convert a Base16 color scheme to an Highlight theme you'll need [mustache]. Mustache is a logic-less templating system which is available in many programming languages.

If you're looking for a quick solution for simply converting Base16 schemes to Highlight themes, you can install these two [Node.js] CLI tools:

- __[mustache.js]__ (by Jan Lehnardt) — JavaScript implementation of [mustache].
- __[yaml-utils]__ (by Christopher Brown) — to convert from YAML to JSON.

In order to build the examples via the scripts in this project, you'll need to install these two packages globally on your machine (obviously, you'll need [Node.js] too).

The CLI syntax to convert a Base16 scheme with these tools is:

```batch
yaml2json < schemename.yaml | mustache - base16_highlight.mustache > schemename.theme
```

... and to produce also a Light version theme (inverted colors in range `base00` to `base07`):

```batch
yaml2json < schemename.yaml | mustache - base16_highlight_light.mustache > schemename.theme
```

For a detailed example, Look at the [`example.bat`][ex.bat] script in this folder.

Of course, you can instead use any mustache tool of your choice to convert Base16 schemes to themes — these mustache templates are not tied to any particular mustache implementation, so they will work with any mustache tool.



# Color Schemes Guidelines

Here are the official Base16 guidelines on how to create color schemes that should fit any syntax, followed by some Highlight-themes specific guidelines.

## Base16 Styling Guidelines

From Base16 Website's _[Styling Guidelines v0.2][B16 Guidelines]_:


> Base16 aims to group similar language constructs with a single color. For example, floats, ints, and doubles would belong to the same colour group. The colors for the default theme were chosen to be easily separable, but scheme designers should pick whichever colours they desire, e.g. `base0B` (green by default) could be replaced with red. There are, however, some general guidelines below that stipulate which `base0B` should be used to highlight each construct when designing templates for editors.
> 
> Since describing syntax highlighting can be tricky, please see [base16-vim] and [base16-textmate] for reference. Though it should be noted that each editor will have some discrepancies due the fact that editors generally have different syntax highlighting engines.
> 
> Colors `base00` to `base07` are typically variations of a shade and run from darkest to lightest. These colors are used for foreground and background, status bars, line highlighting and such. Colors `base08` to `base0F` are typically individual colors used for types, operators, names and variables. In order to create a dark theme, colors `base00` to `base07` should span from dark to light. For a light theme, these colours should span from light to dark.
> 
> ::::::::::::::::::::::::: {.styling}
> 
> - `base00` --- Default Background
> - `base01` --- Lighter Background (Used for status bars)
> - `base02` --- Selection Background
> - `base03` --- Comments, Invisibles, Line Highlighting
> - `base04` --- Dark Foreground (Used for status bars)
> - `base05` --- Default Foreground, Caret, Delimiters, Operators
> - `base06` --- Light Foreground (Not often used)
> - `base07` --- Light Background (Not often used)
> - `base08` --- Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
> - `base09` --- Integers, Boolean, Constants, XML Attributes, Markup Link Url
> - `base0A` --- Classes, Markup Bold, Search Text Background
> - `base0B` --- Strings, Inherited Class, Markup Code, Diff Inserted
> - `base0C` --- Support, Regular Expressions, Escape Characters, Markup Quotes
> - `base0D` --- Functions, Methods, Attribute IDs, Headings
> - `base0E` --- Keywords, Storage, Selector, Markup Italic, Diff Changed
> - `base0F` --- Deprecated, Opening/Closing Embedded Language Tags, e.g. `<?php ?>`
> 
> :::::::::::::::::::::::::

## Highlight Themes Guidelines

Here follows my personal interpretation of the above guidelines to map theme elements to Base16 colors. 

Since some Highlight theme elements map to fixed syntax elements (eg, strings, comments, etc.), but there is no way to predict which elements will be mapped by Keyword groups (this will vary from syntax to syntax), I've therefore chosen a compromise between the original guidelines and what I leaerned by testing different combinations against various syntaxes, and tried to use as many colors of the scheme as possible (leaving out those that usually produce unreadable text in most schemes).

Of course, you are free to interpret the guidelines as you wish, and to rearrange the colors in the templates or the converted themes, but the following mapping is the one I've implemented in the mustache templates:

::::::::::::::::::::::::: {.styling}
- `base00` --- `Canvas`
- `base01` --- (_unused_)
- `base02` --- (_unused_)
- `base03` --- `LineComment` + `BlockComment`
- `base04` --- `LineNum`
- `base05` --- `Default` + `Operator`
- `base06` --- (_unused_)
- `base07` --- (_unused_)
- `base08` --- `Keywords` (group 3)
- `base09` --- `Number` + `Interpolation` + `Keywords` (group 6)
- `base0A` --- `Keywords` (group 4)
- `base0B` --- `String`
- `base0C` --- `Escape` + `StringPreProc`
- `base0D` --- `Keywords` (group 2)
- `base0E` --- `Keywords` (group 1)
- `base0F` --- `PreProcessor` + `Keywords` (group 5)
:::::::::::::::::::::::::

# License

- [`LICENSE`][LICENSE]

This document and the mustache template files in this folder are copyright by Tristano Ajmone, released under the terms of the MIT License:

```
The MIT License

Copyright (c) 2018 Tristano Ajmone: https://gitlab.com/tajmone/highlight-extras
```

The full source code of this document is available in pandoc markdown, along with the scripts used to build and update all the contents of this folder:

- <https://gitlab.com/tajmone/highlight-extras/tree/master/extras/base16>

# Credits

## GitHub Pandoc Template

This document was converted via [pandoc] and [PP], using the __[GitHub Pandoc HTML5 Template]__, which is MIT Licensed:

    Copyright (c) Tristano Ajmone, 2017 (github.com/tajmone/pandoc-goodies)
    Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)
    Copyright (c) 2017 GitHub Inc.

For full details about the license, see the comments in the HTML source of this document.


## Base16 "Eighties"

The following files are derived from the Base16 "Eighties" color scheme, copyright by [Chris Kempson], released under MIT License:

- [`example.yaml`][ex.yaml]
- [`example.theme`][ex.theme]
- [`example_light.theme`][ex-light.theme]

The original scheme was downloaded from:

- <https://github.com/chriskempson/base16-builder>


```
Base16 Builder is released under the MIT License:

Copyright (C) 2012 Chris Kempson

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```

# Links

- [Chris Kempson] website

## Base16

- [Base16 website]
- [Base16 repository]
- [base16-builder] (depracated) — the original Base16 Ruby builder.

Similar Highlight projects:

- [highlight-extras » Base16] — the sources of the 107 Base16 themes found in Highlight's "[`/themes/base16/`][HL base16]" folder. 
- [base16-highlight] (buggy) — Highlight templates by bezhermoso, has [several unresolved issues][B16HL bugs].


## Dependencies

- [mustache] — official website:
    + [mustache(1)] — manual: General commands.
    + [mustache(5)] — manual: File formats and conventions.
- [Node.js]:
    + [mustache.js] — Node implementation of mustache.
    + [yaml-utils] — required to convert YAML schemes to JSON.

<!-----------------------------------------------------------------------------
                               REFERENCE LINKS                                
------------------------------------------------------------------------------>

<!-- Files Links ------------------------------------------------------------->

[LICENSE]:     ./LICENSE "Read the MIT license file"
[MIT License]: ./LICENSE "Read the MIT license file"

[b16HL]:          ./base16_highlight.mustache "Base16 to Highlight theme mustache template (dark version)"
[b16HL light]:    ./base16_highlight_light.mustache "Base16 to Highlight theme mustache template (light version)"
[ex.bat]:         ./example.bat
[ex.pb]:          ./example.pb
[ex.theme]:       ./example.theme
[ex.yaml]:        ./example.yaml
[ex-light.theme]: ./example_light.theme
[ex-dark.html]:   ./example-dark.html
[ex-light.html]:  ./example-light.html

<!-- In-Doc Links ------------------------------------------------------------>

[public domain]: #unlicense "see the 'Unlicense' section"

[released under MIT License]: #credits "see the 'Credits' section"

<!-- highlight-extras -------------------------------------------------------->

[highlight-extras]: https://gitlab.com/tajmone/highlight-extras
[highlight-extras » Base16]: https://gitlab.com/tajmone/highlight-extras/tree/master/base16
[this src]: https://gitlab.com/tajmone/highlight-extras/tree/master/extras/base16 "See the source files used to build this document and its resources..."

[HL base16]: https://gitlab.com/saalen/highlight/tree/master/themes/base16 "See the '/themes/base16/' folder in Highlight’s source repository on GitLab"


<!-- Base16 ------------------------------------------------------------------>

[Chris Kempson]: http://chriskempson.com "Visit Chris Kempson website (chriskempson.com)"

[Base16 website]: http://chriskempson.com/projects/base16/ "Visit the Base16 website"
[Base16 repository]: https://github.com/chriskempson/base16 "Visit the Base16 repository on GitHub"

[base16-builder]: https://github.com/chriskempson/base16-builder

[B16 Guidelines]: http://chriskempson.com/projects/base16/#styling-guidelines
[base16-vim]: https://github.com/chriskempson/base16-vim/
[base16-textmate]: https://github.com/chriskempson/base16-textmate/

[base16-highlight]: https://github.com/bezhermoso/base16-highlight "Visit bezhermoso's 'base16-highlight' repository"
[B16HL bugs]: https://github.com/bezhermoso/base16-highlight/issues/1 "View 'base16-highlight' issue regarding variables naming problems"

<!-- Dependencies ------------------------------------------------------------>

[mustache]: https://mustache.github.io/ "Visit mustache website"
[mustache(1)]: https://mustache.github.io/mustache.1.html "Read mustache man(1)"
[mustache(5)]: https://mustache.github.io/mustache.5.html "Read mustache man(5)"

[mustache.js]: https://www.npmjs.com/package/mustache "Visit 'mustache.js' page on NPM"
[yaml-utils]: https://www.npmjs.com/package/yaml-utils "Visit 'yaml-utils' page on NPM"
[Node.js]: https://nodejs.org/ "Visit Node.js website"

<!-- Miscellanea ------------------------------------------------------------->

[pandoc]: http://pandoc.org/ "Visit the website of pandoc, the universal markup converter"

[PP]: http://cdsoft.fr/pp/ "Visit the website of PP, the generic Preprocessor (with Pandoc in mind)"

<!-- eof -->