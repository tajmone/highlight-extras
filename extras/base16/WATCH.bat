@ECHO OFF
:: This script requires Node.js "multiwatch" to be installed on the system:
::    https://www.npmjs.com/package/multiwatch
ECHO ==============================================================================
ECHO                                BUILD AND WATCH
ECHO ==============================================================================
ECHO A) Buidling README and Example
CALL BUILD_README.bat
ECHO.
ECHO ==============================================================================
ECHO B) Watch files and rebuild on changes
multiwatch    README.markdown ^
              swatches.css ^
    --execute "ECHO -- README CHANGED! & BUILD_README.bat >NUL" ^
              example.bat ^
              example.yaml ^
              base16_highlight.mustache ^
              base16_highlight_light.mustache ^
    --execute "ECHO -- EXAMPLE CHANGED! & example.bat >NUL"
