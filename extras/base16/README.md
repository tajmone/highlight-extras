# Highlight Extras: Base16

The source files used to build the resources I've contributed to Highlight's "[`/extras/themes-resources/base16/`][HL base16]" folder.


# Page Under Construction

This README file still need to be written.

For more info on the contents of this folder, see:

- [`README.html`][README.html]

<!-----------------------------------------------------------------------------
                               REFERENCE LINKS
------------------------------------------------------------------------------>

<!-- Files Links ------------------------------------------------------------->

[LICENSE]:     ./LICENSE "Read the MIT license file"
[MIT License]: ./LICENSE "Read the MIT license file"

[README.html]: ./README.html

[b16HL]:          ./base16_highlight.mustache "Base16 to Highlight theme mustache template (dark version)"
[b16HL light]:    ./base16_highlight_light.mustache "Base16 to Highlight theme mustache template (light version)"
[ex.bat]:         ./example.bat
[ex.pb]:          ./example.pb
[ex.theme]:       ./example.theme
[ex.yaml]:        ./example.yaml
[ex-light.theme]: ./example_light.theme
[ex-dark.html]:   ./example-dark.html
[ex-light.html]:  ./example-light.html

<!----------------------------- Highlight Links ------------------------------>

[HL base16]: https://gitlab.com/saalen/highlight/tree/master/extras/themes-resources/base16 "See the '/extras/themes-resources/base16/' folder in Highlight’s source repository on GitLab"
