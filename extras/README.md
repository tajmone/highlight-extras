# Highlight Extras

The source files used to build the resources I've contributed to Highlight's "[`/extras/`][HL extras]" folder.


-----

**Table of Contents**

<!-- MarkdownTOC autolink="true" bracket="round" autoanchor="false" lowercase="only_ascii" uri_encoding="true" levels="1,2,3" -->

- [Assets Folders](#assets-folders)
- [About These Assets](#about-these-assets)
    - [Pandoc vs GFM Markdown](#pandoc-vs-gfm-markdown)
    - [Requirements](#requirements)

<!-- /MarkdownTOC -->

-----


# Assets Folders

- [`/base16/`][base16] — sources for Highlight's "[`/extras/themes-resources/base16/`][HL base16]" folder.
- [`/css-themes/`][css-themes] — sources for Highlight's "[`/extras/themes-resources/css-themes/`][HL css-themes]" folder.
- [`/langDefs-resources/`][langDefs]  — sources for Highlight's "[`/extras/langDefs-resources/`][HL langDefs]" folder.


Other assets I've contributed to (sources coming soon):

-  `/pandoc/`  — sources for Highlight's "[`/extras/pandoc/`][HL pandoc]" folder.


# About These Assets

Most of these contributed resources come with a "`README.html`" file which is built from markdown using pandoc. The conversion process relies on some common scripts and assets found in "[`../_assets/`][assets]". These workflow simplifies maintainance of the documents and updating the pandoc templates, PP macros, scripts and other shared dependencies.

## Pandoc vs GFM Markdown

To avoid confusion between GFM markdown files (to document the project on GitHub) and pandoc markdown source files (for building the resources' HTML docs), I've adopted the following naming convention:

- "`*.md`" — GitHub Flavored markdown project document.
- "`*.markdown`" — pandoc markdown source document (with PP macros).

## Requirements

In order to convert the documents from markdown to HTML, you'll need:

- [pandoc] — the universal markup converter ([_binaries donwload link_][pandoc dwnld]).
- [PP] — a generic preprocessor for pandoc ([_binaries donwload link_][PP dwnld]).

Some resources might require extra tools to be present in the system — for more details, see the "`README.md`" file in each subfolder. For example:

- [`/css-themes/`][css-themes]:
    + [Sass] — required to build the SCSS boilerplate.




<!-----------------------------------------------------------------------------
                               REFERENCE LINKS
------------------------------------------------------------------------------>

<!--------------------------- Internal Repo Links ---------------------------->

[assets]:     ../_assets/           "navigate to folder..."

[css-themes]: ./css-themes          "navigate to folder..."
[langDefs]:   ./langDefs-resources  "navigate to folder..."
[base16]:     ./base16              "navigate to folder..."

<!----------------------------- Highlight Links ------------------------------>

[HL extras]: https://gitlab.com/saalen/highlight/tree/master/extras "See the '/extras/' folder in Highlight’s source repository on GitLab"
[HL css-themes]: https://gitlab.com/saalen/highlight/tree/master/extras/themes-resources/css-themes "See the '/extras/themes-resources/css-themes/' folder in Highlight’s source repository on GitLab"
[HL base16]: https://gitlab.com/saalen/highlight/tree/master/extras/themes-resources/base16 "See the '/extras/themes-resources/base16/' folder in Highlight’s source repository on GitLab"
[HL langDefs]: https://gitlab.com/saalen/highlight/tree/master/extras/langDefs-resources "See the '/extras/langDefs-resources/' folder in Highlight’s source repository on GitLab"
[HL pandoc]: https://gitlab.com/saalen/highlight/tree/master/extras/pandoc "See the '/extras/pandoc/' folder in Highlight’s source repository on GitLab"

<!------------------------------ Dependencies -------------------.------------>

[pandoc]: http://pandoc.org/ "Visit pandoc website"
[pandoc dwnld]: https://github.com/jgm/pandoc/releases "Pandoc binaries downloads page"

[PP]: http://cdsoft.fr/pp/ "Visit PP website"
[PP dwnld]: http://cdsoft.fr/pp/download.html "PP binaries downloads page"

[Sass]: https://sass-lang.com/ "Visit Sass website"

<!-- eof -->
