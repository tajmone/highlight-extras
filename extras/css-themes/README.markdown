---
title:      Highlight CSS-Themes Examples
toc-title:  CONTENTS
summary: |
    Written by [Tristano Ajmone], Oct 24, 2017, last edited on Jun 13, 2018.
    This document is [released under MIT License] (MIT).

    Latest theme boilerplates, __v1.0__ (2017/10/25):

    - [`hl-theme-boilerplate.scss`][theme scss]
    - [`hl-theme-boilerplate.css`][theme css]

    All theme boilerplates are [public domain].

lang: en
pagetitle:   Highlight CSS-Themes
author-meta: Tristano Ajmone
date-meta:   2017-10-24
...


!comment(ABOUT THIS DOCUMENT)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This is a PP -> Pandoc-Markdown document that requiresthe pp-macros
library from the "Pandoc Goodies" project:

https://github.com/tajmone/pandoc-goodies/tree/master/pp

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Introduction

_Language definition_ files define the syntax of language elements. _Colour themes_ define how these language elements should be formatted in the final highlighted document.

Highlight theme files are Lua files that can handle formatting for various output formats, including HTML. While Highlight themes support basic formatting (_Colour_, _Bold_, _Italic_, and _Underline_), when targetting HTML output you might desire finer control over styling, especially when working in a website, and thus opt to define your custom CSS themes — or use [Sass](http://sass-lang.com/), [Less](http://lesscss.org/), [Stylus](http://stylus-lang.com/), or some other language for compiling CSS.

In order to facilitate creating custom CSS Highlight themes, here you'll find:

1. Some [introductory notes on how Highlight formats its HTML Output][Notes on Highlight HTML Output].
2. A [reference table][Table of Language Elements’ CSS Classes] listing the various **language elements** and their corresponding CSS classes.
3. A [Sass and a CSS boilerplate][Sass/CSS Boilerplates] to start building you custom themes.

# Notes on Highlight HTML Output

Highlight's default behaviour for HTML output is to enclose the code block within a preformatted block with class `hl` (`<pre class="hl">`), and then enclose all the various language elements within `<span>` tags with the `hl` class along the element’s corresponding class.

!GFMAlert
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
__NOTE__: Highlight doesn't nest `<span>` tags in its output!
Before a new tag is opened, the previous one is always closed.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Example source:

``` python
# A very simple Python program
print("Hello World!")
```

Example HTML output:

``` html
<pre class="hl"><span class="hl slc"># A very simple Python program</span>
<span class="hl kwa">print</span><span class="hl opt">(</span><span class="hl str">&quot;Hello World!&quot;</span><span class="hl opt">)</span>
</pre>
```

Line numbers too are handled via a span tag with `lin` class (`<span class="hl lin">`):

``` html
<pre class="hl"><span class="hl lin">    1 </span><span class="hl slc"># A very simple Python program</span>
<span class="hl lin">    2 </span><span class="hl kwa">print</span><span class="hl opt">(</span><span class="hl str">&quot;Hello World!&quot;</span><span class="hl opt">)</span>
</pre>
```


# Table of Language Elements’ CSS Classes

The CSS counterparts of a theme’s `Default` and `Canvas` definitions are the definitions of the `<pre>` tag with `hl` class:

| Element   | CSS                           | Description                      |
|-----------|-------------------------------|----------------------------------|
| `Default` | `pre.hl { ... }`              | Attributes of unspecified text   |
| `Canvas`  | `pre.hl { background: ...; }` | Code block background properties |

The rest of the language elements counterparts (including line numbers) concern `<span>` tags with the following classes:

| Element              | CSS        | Description                            |
|----------------------|------------|----------------------------------------|
| `Number`             | `.hl .num` | Numbers                                |
| `Escape`             | `.hl .esc` | Escape sequences                       |
| `Interpolation`      | `.hl .ipl` | Interpolation sequences                |
| `String`             | `.hl .str` | Strings                                |
| `PreProcessor`       | `.hl .ppc` | Preprocessor directives                |
| `StringPreProc`      | `.hl .pps` | Strings within preprocessor directives |
| `BlockComment`       | `.hl .com` | Block comments                         |
| `LineComment`        | `.hl .slc` | Single-line comments                   |
| `LineNum`            | `.hl .lin` | Line numbers                           |
| `Operator`           | `.hl .opt` | Operators                              |
| `Keywords`&gt;`Id=1` | `.hl .kwa` | Keywords group 1 (`a`)                 |
| `Keywords`&gt;`Id=2` | `.hl .kwb` | Keywords group 2 (`b`)                 |
| `Keywords`&gt;`Id=3` | `.hl .kwc` | Keywords group 3 (`c`)                 |
| `Keywords`&gt;`Id=4` | `.hl .kwd` | Keywords group 4 (`d`)                 |

!GFMAlert
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
__NOTE__: To define CSS `Keywords` groups above `Id=4`, just keep incrementing the last letter in the “`.kw*`” class name — Eg: `.kwe`, `.kwf`, `.kwg`, …, `.kwz`.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Sass/CSS Boilerplates

In this folder you'll find two boilerplate files that you can use as a starting point to build your custom Highlight CSS themes:

- [`hl-theme-boilerplate.scss`][theme scss] --- The Sass SCSS boilerplate.
- [`hl-theme-boilerplate.css`][theme css] --- The CSS boilerplate (built by compiling the previous file).

These boilerplates are useful because they contain all the classes covering Highlight language elements, with comments remind their Lua counterparts names. By using a boilerplate, you'll be reminded to cover all syntax elements, without the need to lookup any tables.

There is also an example HTML document showing how the CSS boilerplate looks like:

- [`example.html`](./example.html)

# Unlicense

With the exception of this HTML document, all files in this folder are released into public domain under the terms of the Unlicense:

- <http://unlicense.org>

# Credits

This documented was created using the __[GitHub Pandoc HTML5 Template]__, which is MIT Licensed:

    Copyright (c) Tristano Ajmone, 2017 (github.com/tajmone/pandoc-goodies)
    Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)
    Copyright (c) 2017 GitHub Inc.

For full details about the license, see the comments in the HTML source of this document.


<!-----------------------------------------------------------------------------
                               REFERENCE LINKS                                
------------------------------------------------------------------------------>

[theme scss]: ./hl-theme-boilerplate.scss
[theme css]:  ./hl-theme-boilerplate.css

[public domain]: #unlicense "see the 'Unlicense' section"

[released under MIT License]: #credits "see the 'Credits' section"


