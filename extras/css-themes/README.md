# Highlight Extras: CSS Themes

The source files used to build the resources I've contributed to Highlight's "[`/extras/themes-resources/css-themes/`][HL css-themes]" folder.


-----

**Table of Contents**

<!-- MarkdownTOC autolink="true" bracket="round" autoanchor="false" lowercase="only_ascii" uri_encoding="true" levels="1,2,3" -->

- [Output Files List](#output-files-list)
- [Source Files List](#source-files-list)
    - [Scripts](#scripts)
    - [README](#readme)
    - [SCSS/CSS Assets](#scsscss-assets)
    - [Example](#example)

<!-- /MarkdownTOC -->

-----

# Output Files List

These source file will build and deploy the following assets to Highlight's "[`/extras/themes-resources/css-themes/`][HL css-themes]" folder.

- `hl-theme-boilerplate.scss` — Sass/SCSS Highlight theme boilerplate.
- `hl-theme-boilerplate.css` — CSS Highlight theme boilerplate.
- `README.html` — assets documentation.
- `example.html` — example of boilerplate usage.
- `UNLICENCE` — public domain [Unlicense] terms.


# Source Files List


## Scripts

- [`BUILD_ALL.bat`][BUILD_ALL.bat] — invokes all the `BUILD_*.bat` scripts at once.
- [`DEPLOY.bat`][DEPLOY.bat] — copies the built assets to local copy of Highlight's repo.<sup>(\*)</sup>
- [`UNLICENCE`][UNLICENCE] — public domain [Unlicense] terms.

> __NOTE<sup>(\*)</sup>__ — the `DEPLOY.bat` script assumes that the locally cloned copy of Highlight's repository ("`highlight`") is in the same directory as this repository. All copy operations rely on relative paths, accordingly:
>
> ```
> common-folder/
>  ├── highlight/            <-- cloned  https://gitlab.com/saalen/highlight
>  │   └── extras/
>  │       └── css-themes/
>  └── highlight-extras/     <-- cloned  https://gitlab.com/tajmone/highlight-extras
>      └── extras/
>          └── css-themes/
> ```
>
> If your clones copy of this repo is not in the same folder of "`highlight`", the script will fail. Adjust the script(s) according to needs.


## README

- [`BUILD_README.bat`][BUILD_README.bat] — conversion script.
- [`README.markdown`][README.markdown] — source file (pandoc markdown + PP macros).
- [`README.html`][README.html] — generated HTML doc.


## SCSS/CSS Assets

- [`BUILD_SCSS.bat`][BUILD_SCSS.bat] — conversion script.
- [`hl-theme-boilerplate.scss`][hl-theme-boilerplate.scss] — SCSS source asset.
- [`hl-theme-boilerplate.css`][hl-theme-boilerplate.css] — CSS converted asset.


## Example

- [`BUILD_EXAMPLE.bat`][BUILD_EXAMPLE.bat] — conversion script.
- [`example.markdown`][example.markdown] — source file (pandoc markdown + PP macros).
- [`example-template.html5`][example-template.html5] — pandoc template.
- [`example.pb`][example.pb] — code example injected into the doc.
- [`example.html`][example.html] — generated HTML doc.




[BUILD_ALL.bat]: ./BUILD_ALL.bat
[BUILD_EXAMPLE.bat]: ./BUILD_EXAMPLE.bat
[BUILD_README.bat]: ./BUILD_README.bat
[BUILD_SCSS.bat]: ./BUILD_SCSS.bat
[DEPLOY.bat]: ./DEPLOY.bat
[example.html]: ./example.html
[example.markdown]: ./example.markdown
[example.pb]: ./example.pb
[example-template.html5]: ./example-template.html5
[hl-theme-boilerplate.css]: ./hl-theme-boilerplate.css
[hl-theme-boilerplate.scss]: ./hl-theme-boilerplate.scss
[README.html]: ./README.html
[README.markdown]: ./README.markdown
[README.md]: ./README.md
[UNLICENCE]: ./UNLICENCE



<!-----------------------------------------------------------------------------
                               REFERENCE LINKS
------------------------------------------------------------------------------>

<!----------------------------- Highlight Links ------------------------------>

[HL extras]: https://gitlab.com/saalen/highlight/tree/master/extras "See the '/extras/' folder in Highlight’s source repository on GitLab"
[HL css-themes]: https://gitlab.com/saalen/highlight/tree/master/extras/themes-resources/css-themes "See the '/extras/themes-resources/css-themes/' folder in Highlight’s source repository on GitLab"

<!--------------------------- External References ---------------------------->

[Unlicense]: http://unlicense.org "Visit unlicense.org"

<!-- eof -->
