@ECHO OFF
:: -----------------------------------------------------------------------------
:: "extras\css-themes\BUILD_README.bat" v1.0.0 (2018/06/22) | by Tristano Ajmone
:: -----------------------------------------------------------------------------
:: Released into the public domain via the Unlicense terms:
::     http://unlicense.org/
:: -----------------------------------------------------------------------------
ECHO ==============================================================================
ECHO             Building Highlight / extras / CSS-Themes / README.html
ECHO ==============================================================================
CALL ..\..\_assets\BUILD_README.bat
ECHO.
EXIT /B
