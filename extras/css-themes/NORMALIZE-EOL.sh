#!/bin/bash
echo -e "\e[94m=============================================================================="
echo -e "\e[93m                              EOL Normalization                               "
echo -e "\e[94m=============================================================================="
# We need this because Dart Sass (as of v1.17.2) mixes EOLs in the final CSS
# when injecting multiline comments:
# https://github.com/sass/dart-sass/issues/623

if [[ $(uname -s) == MINGW* ]];then
	echo -e "\e[93m** Windows OS ** Normalizing CSS files EOL to CRLF..."
	echo -e "\e[90m------------------------------------------------------------------------------\e[32m"
	unix2dos *.css
else
	echo -e "\e[93m** Unix style OS ** Normalizing CSS files EOL to CRLF...\e[32m"
	echo -e "\e[90m------------------------------------------------------------------------------\e[32m"
	dos2unix *.css
fi

echo -e "\e[90m------------------------------------------------------------------------------"
echo -e "\e[93m/// Finished ///\e[0m"

# EOF #
