---
title: Highlight CSS Theme Example
lang: en
author: Tristano Ajmone
date:   October 24, 2017
---

# Custom Highlight CSS Theme Example

This example page uses the "[hl-theme-boilerplate.css]" CSS stylesheet created from the [Sass boilerplate].


!HighlightFile(example.pb)(purebasic)(-l -j 1)

[hl-theme-boilerplate.css]: ./hl-theme-boilerplate.css

[Sass boilerplate]: ./hl-theme-boilerplate.scss

