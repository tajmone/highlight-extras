@ECHO OFF
CLS
:: -----------------------------------------------------------------------------
:: "extras\css-themes\BUILD_ALL.bat" | v1.0.0 (2018/06/22) | by Tristano Ajmone
:: -----------------------------------------------------------------------------
:: Released into the public domain via the Unlicense terms:
::     http://unlicense.org/
:: -----------------------------------------------------------------------------
CALL BUILD_EXAMPLE.bat
CALL BUILD_README.bat
CALL BUILD_SCSS.bat

EXIT /B
