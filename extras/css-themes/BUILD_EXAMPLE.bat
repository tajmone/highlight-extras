@ECHO OFF
:: -----------------------------------------------------------------------------
:: "extras\css-themes\BUILD_EXAMPLE.bat" v1.0.1 (2018/06/22) by Tristano Ajmone
:: -----------------------------------------------------------------------------
:: Released into the public domain via the Unlicense terms:
::     http://unlicense.org/
:: -----------------------------------------------------------------------------
ECHO ==============================================================================
ECHO             Building Highlight / extras / CSS-Themes / Example File
ECHO ==============================================================================
:: This script is used to build an intermediate file for the example:
:: It invokes Highlight on "example.pb" and saves the output as "example_TEMP.html"
:: which is then manually inserted into the body of "example.html"
ECHO.
ECHO 1) Building "example.html" via PP and Pandoc

SET "PP_MACROS_PATH=%~dp0..\..\_assets\pp-macros\"
SET PANDOC_TEMPLATE=example-template.html5

pp  -import %PP_MACROS_PATH%macros.pp ^
    example.markdown ^
    | pandoc  -f markdown+smart+yaml_metadata_block ^
              -t html5 ^
              --template=%PANDOC_TEMPLATE% ^
              --toc ^
              -o example.html

ECHO.
EXIT /B
