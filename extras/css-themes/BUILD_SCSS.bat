:: "BUILD_SCSS.bat"                     v2.0.0 | 2019/03/12 | by Tristano Ajmone
:: -----------------------------------------------------------------------------
:: This script requires Dart Sass to be installed on the system:
::      https://github.com/sass/dart-sass
::
:: You can use Chocolatey to install Dart Sass and keep it updated:
::      https://chocolatey.org/packages/sass
:: -----------------------------------------------------------------------------
@ECHO OFF
:: -----------------------------------------------------------------------------
:: "extras\css-themes\BUILD_SCSS.bat" | v1.0.0 (2018/06/22) | by Tristano Ajmone
:: -----------------------------------------------------------------------------
:: Released into the public domain via the Unlicense terms:
::     http://unlicense.org/
:: -----------------------------------------------------------------------------
ECHO ==============================================================================
ECHO      Building Highlight / extras / CSS-Themes / hl-theme-boilerplate.scss
ECHO ==============================================================================
ECHO.
ECHO 1) Building CSS from SCSS

CALL SASS   --style=expanded ^
            --no-source-map ^
            hl-theme-boilerplate.scss ^
            hl-theme-boilerplate.css
ECHO.
EXIT /B
