# Highlight Extras TODOs

List of pending tasks for the resources of Highlight's "[`/extras/`][HL extras]" folder.


-----

**Table of Contents**

<!-- MarkdownTOC autolink="true" bracket="round" autoanchor="false" lowercase="only_ascii" uri_encoding="true" levels="1,2,3" -->

- [Global TODOs](#global-todos)
- [Add Existing Resources](#add-existing-resources)
- [Extras Resources TODOs](#extras-resources-todos)
    - [Base16](#base16)
- [Create New Resources](#create-new-resources)
- [Notes, Ideas and Reminders](#notes-ideas-and-reminders)

<!-- /MarkdownTOC -->

-----

# Global TODOs

- [x] Use "`*.markdown`" extension (instead of "`*.md`") for pandoc markdown source documents. This allows a clearer distinction between pandoc source files and GitHub documents. After this:
    + [ ] Add "`README.md`" to each assets folder (ie, the source for "`README.html`" will not be "`README.markdown`", and "`README.md`" will be for previewing info about the folder in GitHub WebUI).
- [ ] Create a "`/themes-resources/`" folder and move "`/css-themes/`" and "`/base16/`" into it, as well as other new resources related to themes. I've [proposed this change in the Highlight project], waiting to see if it's approved.

[proposed this change in the Highlight project]: https://gitlab.com/saalen/highlight/merge_requests/75#note_87741185 "See the proposal on GitLab merge request !75"

# Add Existing Resources

- [ ] Import and adapt my other contributed resources:
    + [x]  [`/langDefs-resources/`][langDefs]
    + [ ]  `/pandoc/`

# Extras Resources TODOs

Planned and pending tasks for the resources of Highlight's `extras` folder.

## Base16

- [`./base16/`][base16]

Scripts and mustache templates for converting base16 schemes to Highlight themes:

+ [x] Also create a `light` version of the mustache template (inverts color ramp from `00` to `07`)
+ [ ] Also create a  mustache template for newer base16 standard (uses different variables names), or alternatively:
+ [ ] Create a mustache template to convert a YAML scheme from the newer base16 standard to the old one.
+ [ ] Base16 to Sass/CSS mustache template to create a Highlight stylesheet, using the SCSS templates I've created in [`./css-themes/`][css-themes].

# Create New Resources

Ideas for new resources to put in Highlight's `extras` folder.

# Notes, Ideas and Reminders

Just some memos that it's worth jotting down.

- [ ] PP v2.5 introduced the new `!lua` macro that adds support to send Lua commands. This opens up new possibilities to interact with Highlight scripts (themes and langDefs) and extract values that can be printed into the document. Maybe this could be exploited when creating docs for this project, or even to create some pandoc templates that dynamically interact with Highlight.


<!-----------------------------------------------------------------------------
                               REFERENCE LINKS
------------------------------------------------------------------------------>

<!--------------------------- Internal Repo Links ---------------------------->

[assets]:     ../_assets/           "navigate to folder..."

[css-themes]: ./css-themes          "navigate to folder..."
[langDefs]:   ./langDefs-resources  "navigate to folder..."
[base16]:   ./base16                "navigate to folder..."

<!----------------------------- Highlight Links ------------------------------>

[HL extras]: https://gitlab.com/saalen/highlight/tree/master/extras "See the '/extras/' folder in Highlight’s source repository on GitLab"
[HL css-themes]: https://gitlab.com/saalen/highlight/tree/master/extras/css-themes "See the '/extras/css-themes/' folder in Highlight’s source repository on GitLab"
[HL langDefs]: https://gitlab.com/saalen/highlight/tree/master/extras/langDefs-resources "See the '/extras/langDefs-resources/' folder in Highlight’s source repository on GitLab"
[HL pandoc]: https://gitlab.com/saalen/highlight/tree/master/extras/pandoc "See the '/extras/pandoc/' folder in Highlight’s source repository on GitLab"


<!-- eof -->
