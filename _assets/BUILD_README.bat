@ECHO OFF
:: "_assets\BUILD_README.bat" | v1.1.0 (2018/07/12) | by Tristano Ajmone
:: -----------------------------------------------------------------------------
:: Released into the public domain via the Unlicense terms:
::     http://unlicense.org/
:: -----------------------------------------------------------------------------
:: A shared script to build the "readme.html" file of my contributed resources to
:: Highlight's "/extras/" contents.
:: ------------------------------------------------------------------------------
:: pandoc v2.2.1
:: pp v2.3.6
:: highlight v3.43
:: ------------------------------------------------------------------------------
SET "PP_MACROS_PATH=%~dp0pp-macros\"
ECHO PP_MACROS_PATH:
ECHO   %PP_MACROS_PATH%
ECHO.

SET "PANDOC_TEMPLATE=%~dp0template\GitHub.html5"
ECHO PANDOC_TEMPLATE:
ECHO   %PANDOC_TEMPLATE%

ECHO ------------------------------------------------------------------------------
ECHO Building README.html
ECHO ------------------------------------------------------------------------------
ECHO 1) Converting via PP and Pandoc
pp  -import %PP_MACROS_PATH%macros.pp ^
    README.markdown ^
    %~dp0AFTER_README.markdown ^
    | pandoc  -f markdown+smart+yaml_metadata_block ^
              -t html5 ^
              --template=%PANDOC_TEMPLATE% ^
              --self-contained ^
              --toc ^
              -o README.html
ECHO.
EXIT /B
