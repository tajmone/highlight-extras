:: =============================================================================
::                          DEPLOY FUNCTIONS SNIPPET
:: =============================================================================
:: "_assets\snippet__DEPLOY.bat | "v1.0.2 (2018/07/02) | by Tristano Ajmone
:: -----------------------------------------------------------------------------
:: Released into the public domain via the Unlicense terms:
::     http://unlicense.org/
:: -----------------------------------------------------------------------------
:: THE CODE BELOW IS A SNIPPET CONTAINING HELPER FUNCTIONS FOR BUILDING/UPDATING
:: THE DEPLOY SCRIPTS FOR HIGHLIGHT EXTRAs RESOURCES. SINCE BATCH FILES CAN'T
:: USE FUNCTIONS DEFINED IN OTHER SCRIPTS, THE SNIPPET IS INTEDED TO BE COPIED
:: FROM THIS FILE AND PASTED INTO DEPLOY SCRIPTS. ALL CODE UPDATES SHOULD BE
:: DONE HERE AND THEN COPY-&-PASTED INTO THE BUILD SCRIPTS.
:: -----------------------------------------------------------------------------
@ECHO OFF
CLS

:: SET THE "EXTRAS" SUBFOLDER NAME OF THIS RESOURCE:
SET FOLDER_NAME=foldername

:: INITIALIZE DEPLOY FUNCTIONS:
CALL :INIT

:: DELETE ANY FILES AND FOLDERS IN TARGET:
CALL :ERASE

CALL :BEGIN_DEPLOY

:: FILES TO DEPLOY:
CALL :DEPLOY    README.html
CALL :DEPLOY    UNLICENCE

CALL :SHOW_RESULTS

EXIT /B

:: =============================================================================
::                             DEPLOY FUNCTIONALITY
:: =============================================================================
:: v1.0.2 (2018/07/02)

:INIT
ECHO ==============================================================================
ECHO                         DEPLOY FILES TO HIGHLIGHT REPO
ECHO ==============================================================================
:: The ':INIT' function should be called immediately after setting the value of
:: %FOLDER_NAME%. It initializes other required environment vars and prepares the
:: destination folder by creating it and deleting any previous contents inside it.
:: ------------------------------------------------------------------------------
SET /A CNT=1
SET "HL_XTRAS=..\..\..\highlight\extras"
SET "DEST_DIR=%HL_XTRAS%\%FOLDER_NAME%"
ECHO %CNT%) CREATE DESTINATION FOLDER:
ECHO    %DEST_DIR%
ECHO ------------------------------------------------------------------------------
MD   %DEST_DIR%
SET /A CNT+=1
EXIT /B

:ERASE
ECHO ==============================================================================
ECHO %CNT%) DELETE FILES IN DESTINATION FOLDER:
ECHO ------------------------------------------------------------------------------
DEL /S /Q %DEST_DIR%\*
SET /A CNT+=1
EXIT /B

:BEGIN_DEPLOY
:: This fucntion is just for ornamental purposes: it prints to the console an
:: horizontal divider and the current step header. All calls to ':DEPLOY' will be
:: displayed as entries of this step.
ECHO ==============================================================================
ECHO %CNT%) DEPLOY FILES TO DESTINATION FOLDER:
SET /A CNT+=1
EXIT /B


:DEPLOY
ECHO ------------------------------------------------------------------------------
ECHO DEPLOYING "%1" TO:
ECHO   %DEST_DIR%\%1
COPY "%1" "%DEST_DIR%"
EXIT /B


:SHOW_RESULTS
ECHO ==============================================================================
ECHO %CNT%) SHOW DESTINATION FOLDER'S CONTENTS:
ECHO ------------------------------------------------------------------------------
DIR  %DEST_DIR% /B
ECHO ------------------------------------------------------------------------------
SET /A CNT+=1
EXIT /B
