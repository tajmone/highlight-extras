#!/bin/bash
# "SASS-BUILD.sh "                      v1.0.0 | 2019/03/12 | by Tristano Ajmone
# ------------------------------------------------------------------------------
# This script requires Dart Sass to be installed on the system:
#      https://github.com/sass/dart-sass
#
# You can use Chocolatey to install Dart Sass and keep it updated:
#      https://chocolatey.org/packages/sass
# ------------------------------------------------------------------------------
echo -e "\e[94m=============================================================================="
echo -e "\e[93m                 BUILD CSS STYLESHETS FROM SASS SCSS SOURCES                  "
echo -e "\e[94m=============================================================================="

echo -e "\e[94m1. \e[93mInvoking Sass..."
sass ./:./ --no-source-map
echo -e "\e[90m------------------------------------------------------------------------------"

# ------------------------------------------------------------------------------
#                               EOL Normalization
# ------------------------------------------------------------------------------
# We need this because Dart Sass (as of v1.17.2) mixes EOLs in the final CSS
# when injecting multiline comments:
# https://github.com/sass/dart-sass/issues/623

echo -e "\e[94m2. \e[93mNormalize end-of-line in CSS output:"
if [[ $(uname -s) == MINGW* ]];then
	echo -e "\e[93m** Windows OS ** Normalizing CSS files EOL to CRLF..."
	echo -e "\e[90m------------------------------------------------------------------------------\e[32m"
	unix2dos *.css
else
	echo -e "\e[93m** Unix style OS ** Normalizing CSS files EOL to CRLF...\e[32m"
	echo -e "\e[90m------------------------------------------------------------------------------\e[32m"
	dos2unix *.css
fi

echo -e "\e[90m------------------------------------------------------------------------------"
echo -e "\e[93m/// Finished ///\e[0m"

# EOF #
