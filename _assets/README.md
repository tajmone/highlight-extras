# Shared Assets

This folder contains assets that are reused for building the various resources in this project.


-----

**Table of Contents**

<!-- MarkdownTOC autolink="true" bracket="round" autoanchor="false" lowercase="only_ascii" uri_encoding="true" levels="1,2,3" -->

- [Files and Folders List](#files-and-folders-list)

<!-- /MarkdownTOC -->

-----

# Files and Folders List

- [`/pp-macros/`][pp-macros] — "[PP-Macros Library]" from the __[Pandoc-Goodies]__ project.
- [`/template/`][template] — "[GitHub Pandoc HTML5 Template]" from the __[Pandoc-Goodies]__ project.
- [`AFTER_README.markdown`][AFTER_README.markdown] — an append file used when converting markdown docs, containing common snippets of code.
- [`BUILD_README.bat`][BUILD_README.bat] — a shared script included for building `README.html` files.
- [`snippet__DEPLOY.bat`][snippet__DEPLOY.bat] — a snippet to paste into `DEPLOY.bat` scripts.

The contents of [`/pp-macros/`][pp-macros] and [`/template/`][template] are manually updated from time to time, when new desired features are added in the upstream repository.

The assets from the __[Pandoc-Goodies]__ project are subject to their own license terms (always either MIT License or an MIT compatible license); for more details on their licenses, see their folders' `README` and/or the source files' comments.

<!-----------------------------------------------------------------------------
                               REFERENCE LINKS
------------------------------------------------------------------------------>

<!--------------------------- Internal Repo Links ---------------------------->

[pp-macros]: ./pp-macros
[template]: ./template
[AFTER_README.markdown]: ./AFTER_README.markdown
[BUILD_README.bat]: ./BUILD_README.bat
[snippet__DEPLOY.bat]: ./snippet__DEPLOY.bat

[Pandoc-Goodies]: https://github.com/tajmone/pandoc-goodies "Visit the 'Pandoc-Goodies' project"

[GitHub Pandoc HTML5 Template]: https://github.com/tajmone/pandoc-goodies/tree/master/templates/html5/github  "View the source files at the 'Pandoc-Goodies' project"

[PP-Macros Library]: https://github.com/tajmone/pandoc-goodies/tree/master/pp  "View the source files at the 'Pandoc-Goodies' project"

<!-- eof -->
